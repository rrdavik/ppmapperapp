/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package littlebitz.processing.ppmapper;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

import littlebitz.ui.SwingComponentBuilder;

/**
 *
 * @author David
 */
public class PpMapperApp extends JFrame {
    
    private PpMapper ppMapper;
    private ControlPanel control;
    
    public Dimension screenSize;
//    private ArrayList<JPanel> layers;
     
    public PpMapperApp() {
    	Container contentPane = getContentPane();
    	contentPane.setLayout(new BorderLayout());
    	
        //Create and set up the window.
        setTitle("PPMapper");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    
        
        Point currentPosition = new Point(0, 0);
        screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();

//        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
//        GraphicsDevice [] devices = env.getScreenDevices();
            // REMIND : Multi-monitor full-screen mode not yet supported
//            for (int i = 0; i < 1 /* devices.length */; i++) {
//            dScreenSize = new Dimension((int)devices[0].getDisplayMode().getWidth(),(int)devices[0].getDisplayMode().getHeight());
//                System.out.println("Width "+devices[0].getDisplayMode().getWidth());
//                System.out.println("Height "+devices[0].getDisplayMode().getHeight());
//            }
//        System.out.println("-----------------------*********************+screenSize"+screenSize.toString()); 
//        this.layers = new ArrayList();
        
        
        control = new ControlPanel(currentPosition, new Dimension(300, (int) screenSize.getHeight()), "", new BorderLayout());
        contentPane.add(control, BorderLayout.LINE_START);

     //   currentPosition.x += 300;
        
        JPanel outputPanel = SwingComponentBuilder.createCustomPanel(currentPosition, new Dimension((int) screenSize.getWidth() - 300, (int) screenSize.getHeight()), "", new BorderLayout()); 
        contentPane.add(outputPanel);
      
        ppMapper = new PpMapper();
        ppMapper.init();
        outputPanel.add(ppMapper);
        
    }
    

    public static void main(String[] args) {
               
        PpMapperApp app = new PpMapperApp();
        app.setPreferredSize(app.screenSize);
        
//        (int)outputPanel.getWidth(),  (int)outputPanel.getHeight()
        
        app.pack();
        app.setVisible(true);
        System.out.println("***************app.screenSize"+app.screenSize.toString());
//        app.ppMapper.init();
       
    }
}
