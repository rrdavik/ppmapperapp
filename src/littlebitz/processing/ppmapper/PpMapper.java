// NOTE: You need to manually change the package name on the next line and in the line within the main function.
// This will be automated in future versions once my Netbeans powers have developed...
package littlebitz.processing.ppmapper;

import com.heroicrobot.dropbit.registry.*;
import com.heroicrobot.dropbit.devices.pixelpusher.Pixel;
import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import com.heroicrobot.dropbit.devices.pixelpusher.PixelPusher;

import processing.core.*;
//import processing.opengl.*;
import processing.video.*;
import littlebitz.processing.core.*;
import controlP5.*;

import java.util.List;

/**
 *
 */
public class PpMapper extends PApplet {

    PVector selectedPoint = null;
    SegmentManager sm;

    ControlP5 cp5;

    Hcore core;
    ControlTimer c;
    long doubleclick;
    boolean isSetProcessing;

    DeviceRegistry registry;
    PpObserver testObserver;
    
    public Movie movie;

//    public PpMapper() {
//
//    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PApplet.main(new String[]{"","PpMapperApp"});
    }

    public void setup() {
//    	System.out.println("***************-------------setupPPMAPPER");
        core = new Hcore();
        //  println("processingId "+core.getProcessingID());

      

        registry = new DeviceRegistry();
        testObserver = new PpObserver();
        registry.addObserver(testObserver);

        isSetProcessing = core.isSetProcessing();
//        firstLoop = true;
        sm = new SegmentManager(this);
        sm.load();

        cp5Setup();
         movie = new Movie(this, "tetrahedron.mov");
         movie.loop();
    }
    
    public void firstDraw(){
    	background(0);
//    	size(getParent().getWidth(), getParent().getHeight());
//    	firstLoop=false;
    	System.out.println("***************-------------firstDraw");
    }
   

    public void draw() {
    	System.out.println("***************-------------drawPPMAPPER");
        // TODO: handle each frame of drawing
    	if (frameCount==1){
    		firstDraw();
    	}
        
//         System.out.println("-----------------------*********************+path"+this.sketchPath);
        imageMode(CENTER);
       image(movie,  (displayWidth-300)/2, displayHeight/2);
       
//        imageMode(CORNER);
//       image(movie, 0,0);

   //   scraper();
    }
//    public void init(){
//    	super.init();
//    	System.out.println("***************-------------initPPMAPPER");
//    }
    
    public void  scraper(){
        updatePixels();
    

        if (testObserver.hasStrips && isSetProcessing) {

            registry.setExtraDelay(0);
            registry.startPushing();

            List<Strip> strips = registry.getStrips();

            if (sm.eraseStrips) {
                for (Strip strip : strips) {
                    sm.fillStrip(strip, 0);
                }
                sm.eraseStrips = false;
            }

            for (Segment seg : sm.segments) {
                seg.samplePixels(strips, sm.nStripPixels);
                seg.draw(sm.nStripPixels);
            }

        }
    }
    
    
    public void cp5Setup() {
        cp5 = new ControlP5(this);
        c = new ControlTimer();

    }
    
        
 //Movie events
public void movieEvent(Movie m) {
  m.read();
}


public void exit () {
   sm.save();
   super.exit();
}
    
}
