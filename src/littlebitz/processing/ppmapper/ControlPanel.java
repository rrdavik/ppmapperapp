package littlebitz.processing.ppmapper;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import littlebitz.ui.SwingComponentBuilder;

public class ControlPanel extends JPanel {
	
	public Dimension panelSize;
	public int offset = 22;
	
	private Border labelBorder;
	private Border textFieldBorder;
//	public ArrayList<JLabel> labelList;
//	public ArrayList<JTextField> textFieldList;

	public ControlPanel(Point coords, Dimension d, String name, BorderLayout layout) {
		super(layout);

		Point currentPosition = new Point(0, 0);
		labelBorder = BorderFactory.createEmptyBorder(0,7,0,0);
		textFieldBorder = BorderFactory.createLineBorder(Color.black);
//		labelList = new ArrayList<JLabel>();
//		textFieldList = new ArrayList<JTextField>();
		
		panelSize = d;
		setPreferredSize(panelSize);
		 
		setLocation(coords);
		setBorder(BorderFactory.createTitledBorder(""));
	
		
		GridBagLayout segmentMonitorLayout = new GridBagLayout();
	    JPanel segmentMonitorPanel = SwingComponentBuilder.createCustomPanel(currentPosition, new Dimension((int) panelSize.getWidth(), 50), "Segment Monitor", segmentMonitorLayout );
	    add(segmentMonitorPanel,  BorderLayout.NORTH);
	    segmentMonitorPanel.setBorder(BorderFactory.createTitledBorder("Segment Monitor"));
	    
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
	
		c.weightx = 0.4;
		c.gridx = 0;
		c.gridy = 0;
//		c.ipadx = 4;

		
		JLabel lSizeSeg = SwingComponentBuilder.createColoredLabel("Size", Color.lightGray,  d, labelBorder);
		segmentMonitorLayout.setConstraints(lSizeSeg, c);
		segmentMonitorPanel.add(lSizeSeg);
		
		c.weightx = 0.3;
		c.gridx = 1;
		c.gridy = 0;
		c.insets = new Insets( 1,2,1,0);
		JTextField nSizeSeg = SwingComponentBuilder.createColoredText("1", Color.green,  d,null,textFieldBorder, JTextField.RIGHT);
		segmentMonitorLayout.setConstraints(nSizeSeg, c);
		segmentMonitorPanel.add(nSizeSeg);
		
		c.weightx = 0.4;
		c.gridx = 2;
		c.gridy = 0;
//		c.ipadx = 4;
		JLabel lTotalSeg = SwingComponentBuilder.createColoredLabel("Total", Color.lightGray,  d, labelBorder);
		segmentMonitorLayout.setConstraints(lTotalSeg, c);
		segmentMonitorPanel.add(lTotalSeg);
		
		c.weightx = 0.4;
		c.gridx = 3;
		c.gridy = 0;
		c.insets = new Insets( 1,2,1,0);
		JTextField nTotalSeg = SwingComponentBuilder.createColoredText("0", Color.green,  d,null,textFieldBorder, JTextField.RIGHT);
		segmentMonitorLayout.setConstraints(nTotalSeg, c);
		segmentMonitorPanel.add(nTotalSeg);
		
		
//		label.getPreferredSize();  
//		add(label);
//		currentPosition.x += offset;
//         currentPosition.y += offset;
         
//         JLabel labele = createColoredLabel("Mapped LEDS",
//                 Color.blue, currentPosition);
//         labele.getPreferredSize();  
//    		add(labele);
	}
	
}
