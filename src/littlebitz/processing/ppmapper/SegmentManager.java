/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package littlebitz.processing.ppmapper;

import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import java.awt.Color;
import java.util.ArrayList;
import processing.data.JSONArray;
import processing.data.JSONObject;

/**
 *
 * @author David
 */
public class SegmentManager {

    boolean eraseStrips;
    ArrayList<Segment> segments;
    int currentStrip;
    int currentPP;
    int pixelsMapped;
    int nStripPixels;
    int nUnitPixels;
    int nStripsPP;
    // Por si quisieramos saber el numero de pixeles real conectados
    int pixelsTotal;
    int offset;
    int registryIndex;

    int[] colours;
    PpMapper p;

    SegmentManager(PpMapper sktContainer) {
        p = sktContainer;
        eraseStrips = true;
        segments = new ArrayList<Segment>();
        currentPP = 0;
        currentStrip = 0;
        pixelsMapped = 0;
        nStripPixels = 240;
        nUnitPixels = 48;
        nStripsPP = 8;
        offset = 0;
        registryIndex = 0;

        coloursInit();
    }

    void addPixelsMapped(int nPixels) {
        pixelsMapped += nPixels;
        System.out.println("**********************************************" + offset);
        //if ((nPixels+offset>=nStripPixels) || (nPixels<nStripPixels && offset>0)){
        if (nPixels + offset >= nStripPixels) {
            currentStrip = (pixelsMapped / nStripPixels) - registryIndex * nStripsPP;
        }

        offset = offset + nPixels;
        if ((offset + nPixels) % nUnitPixels == 0) {
            offset = 0;
        }
    }

    void add(String sPixels) {
        int segmentPixels;

        if (sPixels.equals("0.5")) {
            segmentPixels = nUnitPixels / 2;
        } else {
            segmentPixels = nUnitPixels * Integer.valueOf(sPixels);
        }

        Segment seg = new Segment(p, p.mouseX, p.mouseY, p.mouseX, p.mouseY, segmentPixels, colours[currentStrip], currentPP, currentStrip, offset);
        segments.add(seg);
        addPixelsMapped(segmentPixels);
    }

    void add(Segment seg) {
        segments.add(seg);

        int start = seg.strip * p.sm.nUnitPixels + offset;

        //  updateLegend(seg.segmentColor, start, start+seg.pixelCount, seg.pixelCount);
        addPixelsMapped(seg.pixelCount);
    }

    //  void  updateLegend(segmentCovoidlor, start, end, nPixels){
    //    fill(segmentColor);
    //    nostroke();
    //    rect(10,10,10,10);
    //  }
    // }
    int save() {

        JSONArray values = new JSONArray();

        for (Segment seg : segments) {

            JSONObject s = new JSONObject();
            int curIndex = segments.indexOf(seg);

            s.setInt("id", curIndex);
            s.setFloat("sampleStartX", seg.sampleStart.x);
            s.setFloat("sampleStartY", seg.sampleStart.y);
            s.setFloat("sampleStopX", seg.sampleStop.x);
            s.setFloat("sampleStopY", seg.sampleStop.y);
            s.setInt("pp", seg.pp);
            s.setInt("strip", seg.strip);
            s.setInt("pixelOffset", seg.pixelOffset);
            s.setInt("pixelCount", seg.pixelCount);
            s.setInt("segmentColor", seg.segmentColor);

            values.setJSONObject(curIndex, s);
        }

        p.saveJSONArray(values, "data/project.json");
        return 1;
    }

    int load() {
        JSONArray values;
        try {
            values = p.loadJSONArray("data/project.json");

            for (int i = 0; i < values.size(); i++) {

                JSONObject s = values.getJSONObject(i);

                //int id = animal.getInt("id");
                Segment seg = new Segment(p,
                        s.getFloat("sampleStartX"),
                        s.getFloat("sampleStartY"),
                        s.getFloat("sampleStopX"),
                        s.getFloat("sampleStopY"),
                        s.getInt("pixelCount"),
                        s.getInt("segmentColor"),
                        s.getInt("pp"),
                        s.getInt("strip"),
                        s.getInt("pixelOffset")
                );

                add(seg);
            }
        } catch (Exception ex) {
            System.out.println("Exception leyecto json");
        }
        return 1;
    }

    void fillStrip(Strip strip, int customColor) {
        System.out.println("----------------------------fill0 ");
        for (int i = 0; i < strip.getLength(); i++) {
            strip.setPixel(customColor, i);
        }
    }

    void coloursInit() {
        colours = new int[8];
        colours[0] = p.color(255, 0, 0, 255);
        colours[1] = p.color(0, 255, 0, 255);
        colours[2] = p.color(0, 0, 255, 255);
        colours[3] = p.color(255, 255, 255, 255);
        colours[4] = p.color(255, 0, 255, 255);
        colours[5] = p.color(255, 255, 0, 255);
        colours[6] = p.color(0, 255, 255, 255);
        colours[7] = p.color(120, 60, 120, 255);
    }
}
