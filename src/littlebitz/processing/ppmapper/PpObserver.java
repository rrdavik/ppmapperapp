/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package littlebitz.processing.ppmapper;

import java.util.Observer;
import java.util.Observable;

/**
 *
 * @author David
 */
public class PpObserver implements Observer {

    public boolean hasStrips = false;

    PpObserver() {

    }

    public void update(Observable registry, Object updatedDevice) {
        System.out.println("Registry changed!");
        if (updatedDevice != null) {
            System.out.println("Device change: " + updatedDevice);
        }
        this.hasStrips = true;
    }
};
