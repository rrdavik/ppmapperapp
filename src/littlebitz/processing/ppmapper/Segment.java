/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package littlebitz.processing.ppmapper;

import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import java.util.List;
import processing.core.*;

/**
 *
 * @author David
 */
public class Segment {

    PVector sampleStart;
    PVector sampleStop;
    int pp;
    int strip;
    int pixelOffset = 0;
    int pixelCount = 0;
    int segmentColor;
    PpMapper p;

//  Segment(float startX, float startY, float stopX, float stopY, Strip strip, int pixelCount, int pixelOffset ){
//    this( startX, startY, stopX, stopY, strip );
//    pixelCount = pixelCount;
//    pixelOffset = pixelOffset;
//  }
//
//  Segment(float startX, float startY, float stopX, float stopY, Strip strip ){
//    sampleStart = new PVector(startX, startY);
//    sampleStop = new PVector(stopX, stopY);
//    strip = strip;
//    pixelCount = strip.getLength();
//    System.out.println("Pixels!!! : " + pixelCount);
//  }
    Segment(PpMapper sktContainer, float startX, float startY, float stopX, float stopY, int nPixelCount, int nSegmentColor, int currentPP, int currentStrip, int offset) {
        p = sktContainer;
        sampleStart = new PVector(startX, startY);
        sampleStop = new PVector(stopX, stopY);
        pp = currentPP;
        strip = currentStrip;
        //pixelCount = strip.getLength();
        pixelCount = nPixelCount;
        pixelOffset = offset;
        segmentColor = nSegmentColor;
        System.out.println("New Segment. Pixels!!! : " + pixelCount + " Strip: " + strip + " pp: " + pp + " offset: " + pixelOffset);
    }

    // draw end points and sample points.
    public void draw(int nStripPixels) {
        p.stroke(100);
        p.noFill();

        // draw circles at the end points.
        p.ellipse(sampleStart.x, sampleStart.y, 8, 8);
        p.ellipse(sampleStop.x, sampleStop.y, 8, 8);

        PVector step = PVector.sub(sampleStop, sampleStart);
        step.div(pixelCount);

        PVector samplePos = new PVector();
        samplePos.set(sampleStart);

        p.noStroke();
        p.fill(segmentColor);
        for (int i = 0; i < pixelCount; i++) {

            p.ellipse(samplePos.x, samplePos.y, new Float(3.5).floatValue(), new Float(3.5).floatValue());
            samplePos.add(step);
//      System.out.println("----------------------------------------------------------"+(pixelOffset+i));
//      if (pixelOffset+i>nStripPixels){
// fill(segmentColor+1);
//      }

        }
    }

    // sample pixels and push them to a strip.
    public void samplePixels(List<Strip> strips, int nStripPixels) {

        PVector step = PVector.sub(sampleStop, sampleStart);
        step.div(pixelCount);

        PVector samplePos = new PVector();
        samplePos.set(sampleStart);

        if (strip < strips.size()) {
            int currentStrip = strip;
            int multiplo = 0;
            Strip oStrip = strips.get(currentStrip);
            for (int i = 0; i < pixelCount; i++) {
                if (pixelOffset + i > nStripPixels) {
                    System.out.println("----------------------------------------------------------" + (pixelOffset + i));
                    oStrip.setPixel(p.get((int) samplePos.x, (int) samplePos.y), i - (multiplo * nStripPixels) + pixelOffset - nStripPixels);
                } else {
                    oStrip.setPixel(p.get((int) samplePos.x, (int) samplePos.y), i - (multiplo * nStripPixels) + pixelOffset);
                }
                samplePos.add(step);
                if ((i > 0 && i % nStripPixels == 0) || (pixelCount < nStripPixels && pixelOffset == 24 && i == 24)) {

                    oStrip = strips.get(++currentStrip);
                }
                if (i > 0 && i % nStripPixels == 0) {
                    multiplo = (i / nStripPixels);
                }

            }
            multiplo = 0;
        }

    }

    public String toString() {
        
        return sampleStart.x + ","
                + sampleStart.y + ","
                + sampleStop.x + ","
                + sampleStop.y + ","
                + pp + ","
                + strip + ","
                + pixelOffset + ","
                + pixelCount + ","
                + segmentColor;

    }
}
