package littlebitz.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

  public  class SwingComponentBuilder {

	  //Create the control pane for the top of the frame.
	    public static JPanel createCustomPanel(Point coords, Dimension d, String name, LayoutManager layout) {

	        JPanel panel = new JPanel(layout);
	        if (d != null) {
	       //     panel.setSize(d);
	            panel.setPreferredSize(d);
	        }
	        panel.setLocation(coords);

	        panel.setName(name);

	        panel.setBorder(BorderFactory.createTitledBorder(name));
	        return panel;
	    }
	    
	  //Create and set up a colored label.
	    public static JLabel createColoredLabel(String text,
	            Color color,
	            Dimension d,
	            Border b){

	        JLabel label = new JLabel(text);
	        label.setVerticalAlignment(JLabel.TOP);
	        label.setHorizontalAlignment(JLabel.LEFT);
	        label.setOpaque(true);
	        label.setBackground(color);
	        label.setForeground(Color.black);
	        label.setBorder(b);
	        
//	        label.setBounds(0, 0 140, 140);
	        return label;
	    }
	    
	    //Create and set up a colored label.
	    public static JTextField createColoredText(String text,
	            Color color,
	            Dimension d,
	            Integer cols,
	            Border b,
	            int align){
	    	
	    	JTextField t = new JTextField(text);
	    	if (cols!=null){
	    		t.setColumns(cols);
	    	}
	
		        t.setHorizontalAlignment(align);
		        t.setOpaque(true);
		        t.setBackground(color);
		        t.setForeground(Color.black);
		        t.setBorder(b);
	    	return t;
	    	
	    }
//        onTop = new JCheckBox("Top Position in Layer");
//        onTop.setSelected(true);
//        onTop.setActionCommand(ON_TOP_COMMAND);
//        onTop.addActionListener(this);
//
//        layerList = new JComboBox(layerIds);
//        layerList.setSelectedIndex(INITIAL_DUKE_LAYER_INDEX);
//        layerList.setActionCommand(LAYER_COMMAND);
//        layerList.addActionListener(this);
}
